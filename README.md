# Enigma task
This program allows to send encrypted messages to users. All data is encrypted.
Nobody will be able to access your data.

The next files are encrypted:
users.dat, tokens.dat, invites.dat, web\...
They are located in \enigma-task\Enigma\bin\Debug\netcoreapp3.1.
Also you can change this file paths in ApplicationResources.cs

# How to use it?
1. You need to authenticate (sign up)

![authenticate image](https://i.imgur.com/FAR8Eqv.png)

2. Sign in

![sign in image](https://i.imgur.com/5olvt9z.png)

3. Send invite to user that you want to message.

![send invite image](https://i.imgur.com/StmgrVY.png)

If you try to send message to user without invite, you will get error message like this:

![error mes image](https://i.imgur.com/sxGD1PC.png)

4. User needs to accept invite

![accept invite image](https://i.imgur.com/jyAD9xO.png)

![accept invite 2 image](https://i.imgur.com/YDNoFkl.png)

5. You can send message to user, but first you have to create token. It is create only once. Token must be unique!

![send mes image](https://i.imgur.com/IfEXbdJ.png)

6. You can delete yours messages

![delete mes image](https://i.imgur.com/maTaAiD.png)

![delete mes 2 image](https://i.imgur.com/w1Aybvx.png)

# Task definition
Представим, что у нас есть локальная сеть, 
общее пространство которой представленно какой-то папкой, 
указанной в определенном месте программы.
(EX: C:\Users\Username\Documents)

Наша задача - создать программу, которая позволяет 
	пользователям посылать друг другу зашифрованные сообщения, 
	посредством любого формата файла,
	где один файл == одному сообщению от одного пользователя другому в папке, 
	где папка == пользователь

Основной алгоритм - Шифр Цезаря. Ключи пользователи выбирают сами, 
	но они должны быть уникальным для каждого диалога

1) Аутентификация. Пользователь может регистроваться и входить под своим именем.
	Реализация простая - имя/пароль, сохранение пользователя в файл + шифровка, 
	чтобы никто, кроме программы, не смог прочесть данные.
2) Пользователю доступная исходная папка с "локальным интернетом" - web.
	В папке находится список других пользователей, 
	которым можно отправить сообщение, но только при условии согласия и 
	создания общего ключа.
3) Сообщения можно удалить только тому пользователю, который состоит в диалоге.
