﻿namespace Enigma.Enums
{
    public enum MenuLoginType
    {
        Exit,
        Authentication,
        Authorization,
        AboutProgram
    }
}
