﻿namespace Enigma.Enums
{
    public enum MenuUserType
    {
        Back,
        SendMessage,
        ShowMessages,
        SendInvite,
        AcceptInvite,
        ShowInvites,
        DeleteMessage
    }
}
