﻿namespace Enigma.Interfaces
{
    public interface IFileService
    {
        public void WriteFile();

        public void ReadFile();
    }
}
