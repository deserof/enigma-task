﻿using System.Collections.Generic;
using System.IO;

namespace Enigma
{
    public static class FileHelper
    {
        public static List<string> GetAllFiles()
        {
            if (!Directory.Exists(ApplicationResources.UsersDirectory))
            {
                return new List<string>();
            }

            return new List<string>(Directory.GetFiles(ApplicationResources.UsersDirectory, "*.*", SearchOption.AllDirectories));
        }
    }
}
