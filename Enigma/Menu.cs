﻿using System;
using Enigma.Enums;
using Enigma.Implementations;
using Enigma.Services;

namespace Enigma
{
    public class Menu
    {
        private UserService userService = new UserService();
        private MessageService messageService = new MessageService();
        private InviteService inviteService = new InviteService();
        private InviteFileService inviteFileService = new InviteFileService();
        private MessageFileService messageFileService = new MessageFileService();
        private TokenFileService tokenFileService = new TokenFileService();

        public void LoginMenu()
        {
            bool isMenuOpen = true;

            while (isMenuOpen)
            {
                Console.WriteLine(ApplicationResources.LoginMenu);
                Console.WriteLine(ApplicationResources.MenuTextAuthentication);
                Console.WriteLine(ApplicationResources.MenuTextAuthorization);
                Console.WriteLine(ApplicationResources.MenuTextAboutProgram);
                Console.WriteLine(ApplicationResources.MenuTextExit);
                MenuLoginType menuLoginType = (MenuLoginType)GetMenuNumber();

                switch (menuLoginType)
                {
                    case MenuLoginType.Authentication:
                        {
                            Console.Clear();
                            userService.DoAuthenticationUser();
                            break;
                        }
                    case MenuLoginType.Authorization:
                        {
                            Console.Clear();
                            userService.DoAuthorizationUser();
                            inviteFileService.ReadFile();
                            messageFileService.ReadFile();
                            tokenFileService.ReadFile();
                            UserMenu();
                            break;
                        }
                    case MenuLoginType.AboutProgram:
                        {
                            Console.Clear();
                            MenuAboutProgram();
                            break;
                        }
                    case MenuLoginType.Exit:
                        {
                            Console.Clear();
                            return;
                        }
                    default:
                        {
                            Console.WriteLine(ApplicationResources.WrongInputText);
                            break;
                        }
                }
            }
        }

        private void MenuAboutProgram()
        {
            bool isMenuOpen = true;

            while (isMenuOpen)
            {
                Console.WriteLine(ApplicationResources.AboutProgramText);
                Console.WriteLine(ApplicationResources.MenuTextBack);
                MenuAboutProgramType menuAboutProgramType = (MenuAboutProgramType)GetMenuNumber();

                switch (menuAboutProgramType)
                {
                    case MenuAboutProgramType.back:
                        {
                            return;
                        }
                    default:
                        {
                            Console.WriteLine(ApplicationResources.WrongInputText);
                            break;
                        }
                }
            }
        }

        private void UserMenu()
        {
            bool isMenuOpen = true;

            while (isMenuOpen)
            {
                Console.WriteLine(ApplicationResources.UserMenu);
                Console.WriteLine(ApplicationResources.MenuTextSendMessage);
                Console.WriteLine(ApplicationResources.MenuTextShowMessages);
                Console.WriteLine(ApplicationResources.MenuTextSendInvite);
                Console.WriteLine(ApplicationResources.MenuTextAcceptInvite);
                Console.WriteLine(ApplicationResources.MenuTextShowInvites);
                Console.WriteLine(ApplicationResources.MenuTextDeleteMessage);
                Console.WriteLine(ApplicationResources.MenuTextBack);
                MenuUserType menuUserType = (MenuUserType)GetMenuNumber();

                switch (menuUserType)
                {
                    case MenuUserType.SendMessage:
                        {
                            Console.Clear();
                            messageService.SendMessage();
                            break;
                        }
                    case MenuUserType.ShowMessages:
                        {
                            Console.Clear();
                            messageService.ShowDialogs();
                            break;
                        }
                    case MenuUserType.DeleteMessage:
                        {
                            Console.Clear();
                            messageService.DeleteMessage();
                            break;
                        }
                    case MenuUserType.SendInvite:
                        {
                            Console.Clear();
                            inviteService.SendInvite();
                            break;
                        }
                    case MenuUserType.AcceptInvite:
                        {
                            Console.Clear();
                            inviteService.AcceptInvite();
                            break;
                        }
                    case MenuUserType.ShowInvites:
                        {
                            Console.Clear();
                            inviteService.ShowInvites();
                            break;
                        }
                    case MenuUserType.Back:
                        {
                            Console.Clear();
                            userService.Logout();
                            return;
                        }
                    default:
                        {
                            Console.Clear();
                            Console.WriteLine(ApplicationResources.WrongInputText);
                            break;
                        }
                }
            }
        }

        private int GetMenuNumber()
        {
            int menuNumber;
            Console.WriteLine(ApplicationResources.MenuTextEnterMenuNumber);

            while (!int.TryParse(Console.ReadLine(), out menuNumber) || menuNumber < 0)
            {
                Console.WriteLine(ApplicationResources.WrongInputText);
            }

            return menuNumber;
        }
    }
}
