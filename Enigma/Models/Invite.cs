﻿namespace Enigma.Models
{
    public class Invite
    {
        public string Sender { get; set; }

        public string Receiver { get; set; }

        public bool IsInviteAccpept { get; set; } = false;

        public Invite(string firstUser, string secondUser, bool isInviteAccpept)
        {
            Sender = firstUser;
            Receiver = secondUser;
            IsInviteAccpept = isInviteAccpept;
        }
    }
}
