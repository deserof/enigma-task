﻿namespace Enigma.Models
{
    public class Token
    {
        public string FirstUser { get; set; }

        public string SecondUser { get; set; }

        public int TokenValue { get; set; }

        public Token(string firstUser, string secondUser, int tokenValue)
        {
            FirstUser = firstUser;
            SecondUser = secondUser;
            TokenValue = tokenValue;
        }
    }
}
