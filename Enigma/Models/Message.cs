﻿using System;

namespace Enigma.Models
{
    public class Message
    {
        public string MessageText { get; set; }

        public string Sender { get; set; }

        public string Receiver { get; set; }

        public DateTime DateTime { get; set; }

        public Message(string sender, string receiver, string messageText)
        {
            Sender = sender;
            Receiver = receiver;
            MessageText = messageText;
            DateTime = DateTime.Now;
        }

        public Message(string sender, string receiver, string messageText, DateTime dateTime)
        {
            Sender = sender;
            Receiver = receiver;
            MessageText = messageText;
            DateTime = dateTime;
        }

        public Message(string sender, string receiver, DateTime dateTime)
        {
            Sender = sender;
            Receiver = receiver;
            DateTime = dateTime;
        }
    }
}
