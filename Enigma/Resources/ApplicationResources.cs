﻿namespace Enigma
{
    public static class ApplicationResources
    {
        public const string UsersFile = "users.dat";
        public const string TokensFile = "tokens.dat";
        public const string InvitationsFile = "invites.dat";
        public const string MessageFileExtension = ".dat";
        public const string UsersFolder = @"web\";
        public const string UsersDirectory = @"web";
        public const string DateTimeFormat = "ddMMyyHHmmss";
        public const string ChooserUserThatYouWantToSendInvite = "Chooser user that you want to send invite";
        public const string HasAcceptedInvite = "has accepted invite";
        public const string YouAlreadyHaveSentInviteOrYouHaveIncomingInvite = "You already have sent invite or you have incoming invite";
        public const string ChooseInviteThatYouWantToAccept = "Choose invite id that you want to accept";
        public const string InviteAccepted = "Invite: Accepted";
        public const string InviteNotAccepted= "Invite: Not accepted";
        public const string InviteIsAccepted = "Invite is accepted";
        public const string FirstYouNeedToSendInviteTo = "First, you need to send invite to";
        public const string HasNotYetAcceptedTheInvitation = "has not yet accepted the invitation";
        public const string EnterMessageText = "Enter message text";
        public const string Dialogues = "Dialogues";
        public const string ChooseUserToShowDialogue = "Choose user to show dialogue";
        public const string ChooseIdMessageToDelete = "Choose id message to delete (only yours)";
        public const string Id = "Id:";
        public const string Sender = "Sender:";
        public const string Receiver = "Receiver:";
        public const string MessageText = "Message text:";
        public const string TokenIsNotExisted = "Token is not existed";
        public const string ConsoleTitleText = "Enigma Task";
        public const int AesEncryptionBlockSize = 128;
        public const int AesEncryptionKeySize = 256;
        public const string LoginMenu = "---Login menu---";
        public const string MenuTextAuthentication = "1. Authentication";
        public const string MenuTextAuthorization = "2. Authorization";
        public const string MenuTextAboutProgram = "3. About program";
        public const string MenuTextExit = "0. Exit";
        public const string MenuTextBack = "0. Back";
        public const string UserMenu = "---User menu---";
        public const string MenuTextSendMessage = "1. Send message";
        public const string MenuTextShowMessages = "2. Show messages";
        public const string MenuTextSendInvite = "3. Send invite";
        public const string MenuTextAcceptInvite = "4. Accept invite";
        public const string MenuTextShowInvites = "5. Show incoming invites";
        public const string MenuTextDeleteMessage = "6. Delete message";
        public const string MenuTextEnterMenuNumber = ">> Enter menu number <<";
        public const string WrongInputText = "Wrogn input. Try again";
        public const string Alfabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ .,!?:-'";
        public const string EnterNameText = "Enter name";
        public const string EnterPasswordText = "Enter password";
        public const string UserExistedText = "User is existed";
        public const string ChooseUserYouWantToMessageText = "Choose user id, that you want send message";
        public const string EnterUniqueTokenText = "Enter unique token";
        public const string UserIsNotFoundText = "User is not found";
        public const string LoginUserIsNotFoundText = "Login user is not found";
        public const string IdUserColumnText = "Id User_name";
        public const string AboutProgramText = @"Hello.This program allows to send encrypted messages to users. All data is encrypted.
Nobody will be able to access your data.

The next files are encrypted:
users.dat, tokens.dat, invites.dat, web\...
They are located in \enigma-task\Enigma\bin\Debug\netcoreapp3.1.
Also you can change this file paths in ApplicationResources.cs

How to use it?
1. You need to authenticate (sign up)
2. Sign in
3. Send invite to user that you want to message.
If you try to send message to user without invite, you will get error message
4. User needs to accept invite
5. You can send message to user, but first you have to create token. It is create only once.
Token must be unique!
6. You can delete yours messages.";
    }
}
