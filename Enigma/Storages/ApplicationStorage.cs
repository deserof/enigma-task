﻿using System.Collections.Generic;
using Enigma.Models;

namespace Enigma.Storages
{
    public static class ApplicationStorage
    {
        public static List<User> users = new List<User>();

        public static List<Message> messages = new List<Message>();

        public static List<Token> tokens = new List<Token>();

        public static List<Invite> invites = new List<Invite>();
    }
}
