﻿using System;

namespace Enigma
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.Title = ApplicationResources.ConsoleTitleText;
            Menu menu = new Menu();
            menu.LoginMenu();
        }
    }
}
