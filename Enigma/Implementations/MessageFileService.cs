﻿using Enigma.Interfaces;
using Enigma.Models;
using Enigma.Storages;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace Enigma.Implementations
{
    class MessageFileService : IFileService
    {
        public void ReadFile()
        {
            List<string> files = new List<string>(FileHelper.GetAllFiles());

            if (files.Count == 0)
            {
                return;
            }

            ApplicationStorage.messages.Clear();
            List<string> senders = new List<string>();
            List<string> receivers = new List<string>();
            List<DateTime> dateTimes = new List<DateTime>();

            for (int i = 0; i < files.Count; i++)
            {
                string sender = Regex.Match(files[i], @"\\(.*?)\\").Groups[1].Value;
                senders.Add(sender);
                string receiver = Regex.Match(files[i], @"\\" + sender + @"\\[0-9]*_(.*?)\" + ApplicationResources.MessageFileExtension).Groups[1].Value;
                receivers.Add(receiver);
                string dateTimeText = Regex.Match(files[i], @"\\" + sender + @"\\(.*?)_" + receiver + @"\" + ApplicationResources.MessageFileExtension).Groups[1].Value;
                DateTime dateTime = Convert.ToDateTime(DateTime.ParseExact(dateTimeText, ApplicationResources.DateTimeFormat, CultureInfo.InvariantCulture));
                dateTimes.Add(dateTime);
            }

            try
            {
                for (int i = 0; i < senders.Count; i++)
                {
                    string messageText = string.Empty;
                    string path = ApplicationResources.UsersFolder + senders[i] + @"\" + dateTimes[i].ToString(ApplicationResources.DateTimeFormat, CultureInfo.InvariantCulture) +
                        "_" + receivers[i] + ApplicationResources.MessageFileExtension;

                    using (StreamReader streamReader = new StreamReader(path, Encoding.Default))
                    {
                        while ((messageText = streamReader.ReadLine()) != null)
                        {
                            ApplicationStorage.messages.Add(new Message(senders[i], receivers[i], messageText, dateTimes[i]));
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
        }

        public void WriteFile()
        {
            foreach (var message in ApplicationStorage.messages)
            {
                string path = ApplicationResources.UsersFolder + message.Sender + @"\" + message.DateTime.ToString(ApplicationResources.DateTimeFormat,
                    CultureInfo.InvariantCulture) + "_" + message.Receiver + ApplicationResources.MessageFileExtension;

                try
                {
                    using (StreamWriter streamWriter = new StreamWriter(path, false, Encoding.Default))
                    {
                        streamWriter.WriteLine(message.MessageText);
                    }
                }

                catch (Exception exception)
                {
                    Console.WriteLine(exception.Message);
                }
            }
        }

        public void DeleteMessageFile(Message message)
        {
            string path = ApplicationResources.UsersFolder + message.Sender + @"\" + message.DateTime.ToString(ApplicationResources.DateTimeFormat, 
                CultureInfo.InvariantCulture) + "_" + message.Receiver + ApplicationResources.MessageFileExtension;
            File.Delete(path);
        }
    }
}
