﻿using Enigma.Interfaces;
using Enigma.Models;
using Enigma.Storages;
using System;
using System.IO;
using System.Text;

namespace Enigma.Implementations
{
    class TokenFileService : IFileService
    {
        private IEncryption encryption = new AesEncryption();

        public void ReadFile()
        {
            if (!File.Exists(ApplicationResources.TokensFile))
            {
                return;
            }

            ApplicationStorage.tokens.Clear();

            try
            {
                using (StreamReader streamReader = new StreamReader(ApplicationResources.TokensFile, Encoding.Default))
                {
                    string line;

                    while ((line = streamReader.ReadLine()) != null)
                    {
                        string[] lineArray = encryption.Decrypt(line).Split(' ');
                        string firstUser = lineArray[0];
                        string secondUser = lineArray[1];
                        int tokenValue = Convert.ToInt32(lineArray[2]);

                        ApplicationStorage.tokens.Add(new Token(firstUser, secondUser, tokenValue));
                    }
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
        }

        public void WriteFile()
        {
            try
            {
                using (StreamWriter streamWriter = new StreamWriter(ApplicationResources.TokensFile, true, Encoding.Default))
                {
                    foreach (var token in ApplicationStorage.tokens)
                    {
                        string text = token.FirstUser + " " + token.SecondUser + " " + token.TokenValue;
                        streamWriter.WriteLine(encryption.Encrypt(text));
                    }
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
        }
    }
}
