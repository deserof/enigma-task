using System;
using System.Text;
using System.Security.Cryptography;
using Enigma.Interfaces;

namespace Enigma.Implementations
{
    public class AesEncryption : IEncryption
    {
        private AesCryptoServiceProvider cryptoServiceProvider = new AesCryptoServiceProvider();
        private byte[] key = { 35, 108, 2, 25, 40, 101, 250, 181, 44, 32, 44, 152, 188, 91, 16, 143, 13, 3, 38, 165, 44, 250, 215, 221, 18, 189, 101, 11, 145, 43, 183, 192 };
        private byte[] IV = { 45, 157, 194, 181, 135, 220, 122, 21, 76, 233, 41, 11, 186, 233, 213, 191 };

        public AesEncryption()
        {
            cryptoServiceProvider.BlockSize = ApplicationResources.AesEncryptionBlockSize;
            cryptoServiceProvider.KeySize = ApplicationResources.AesEncryptionKeySize;
            cryptoServiceProvider.IV = IV;
            cryptoServiceProvider.Key = key;
            cryptoServiceProvider.Mode = CipherMode.CBC;
            cryptoServiceProvider.Padding = PaddingMode.PKCS7;
        }

        public string Decrypt(string text)
        {
            ICryptoTransform transform = cryptoServiceProvider.CreateDecryptor();
            byte[] encryptedBytes = Convert.FromBase64String(text);
            byte[] decryptedBytes = transform.TransformFinalBlock(encryptedBytes, 0, encryptedBytes.Length);
            string decryptedText = ASCIIEncoding.UTF8.GetString(decryptedBytes);
            return decryptedText;
        }

        public string Encrypt(string text)
        {
            ICryptoTransform transform = cryptoServiceProvider.CreateEncryptor();
            byte[] encryptedBytes = transform.TransformFinalBlock(ASCIIEncoding.UTF8.GetBytes(text), 0, text.Length);
            string encryptedText = Convert.ToBase64String(encryptedBytes);
            return encryptedText;
        }
    }
}
