﻿using Enigma.Interfaces;
using Enigma.Models;
using Enigma.Storages;
using System;
using System.IO;
using System.Text;

namespace Enigma.Implementations
{
    class InviteFileService : IFileService
    {
        private IEncryption encryption = new AesEncryption();

        public void ReadFile()
        {
            if (!File.Exists(ApplicationResources.InvitationsFile))
            {
                return;
            }

            ApplicationStorage.invites.Clear();

            try
            {
                using (StreamReader streamReader = new StreamReader(ApplicationResources.InvitationsFile, Encoding.Default))
                {
                    string line;

                    while ((line = streamReader.ReadLine()) != null)
                    {
                        string[] lineArray = encryption.Decrypt(line).Split(' ');
                        string firstUser = lineArray[0];
                        string secondUser = lineArray[1];
                        bool acceptInvitation = Convert.ToBoolean(lineArray[2]);

                        ApplicationStorage.invites.Add(new Invite(firstUser, secondUser, acceptInvitation));
                    }
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
        }

        public void WriteFile()
        {
            try
            {
                using (StreamWriter streamWriter = new StreamWriter(ApplicationResources.InvitationsFile, false, Encoding.Default))
                {
                    foreach (var invite in ApplicationStorage.invites)
                    {
                        string text = invite.Sender + " " + invite.Receiver + " " + invite.IsInviteAccpept;
                        streamWriter.WriteLine(encryption.Encrypt(text));
                    }
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
        }
    }
}
