﻿using Enigma.Interfaces;
using System.IO;
using Enigma.Storages;
using Enigma.Models;
using System;

namespace Enigma.Implementations
{
    class BinaryFileService : IFileService
    {
        private IEncryption encryption = new AesEncryption();

        public void ReadFile()
        {
            if (!File.Exists(ApplicationResources.UsersFile))
            {
                return;
            }

            ApplicationStorage.users.Clear();

            try
            {
                using (BinaryReader binaryReader = new BinaryReader(File.Open(ApplicationResources.UsersFile, FileMode.OpenOrCreate)))
                {
                    while (binaryReader.PeekChar() > -1)
                    {
                        string name = binaryReader.ReadString();
                        string password = binaryReader.ReadString();
                        ApplicationStorage.users.Add(new User(encryption.Decrypt(name), encryption.Decrypt(password)));
                    }
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
        }

        public void WriteFile()
        {
            try
            {
                using (BinaryWriter binaryWriter = new BinaryWriter(File.Open(ApplicationResources.UsersFile, FileMode.Create)))
                {
                    foreach (User item in ApplicationStorage.users)
                    {
                        binaryWriter.Write(encryption.Encrypt(item.Name));
                        binaryWriter.Write(encryption.Encrypt(item.Password));
                    }
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
        }
    }
}
