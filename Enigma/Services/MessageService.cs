﻿using System;
using Enigma.Storages;
using Enigma.Models;
using Enigma.Implementations;

namespace Enigma.Services
{
    public class MessageService
    {
        private UserService userService = new UserService();
        private TokenService tokenService = new TokenService();
        private MessageFileService messageFileService = new MessageFileService();
        private InviteService inviteService = new InviteService();

        public void SendMessage()
        {
            userService.ShowUserList();
            Console.WriteLine(ApplicationResources.ChooseUserYouWantToMessageText);
            object userId = userService.GetUserId();

            if (userId is null)
            {
                return;
            }

            int userIdValue = (int)userId;
            string firstUser = userService.GetLoginUserName();
            string secondUser = userService.GetUserNameById(userIdValue);

            if (!inviteService.IsInviteAccept(firstUser, secondUser))
            {
                Console.WriteLine($"{ApplicationResources.FirstYouNeedToSendInviteTo} {secondUser}");
                return;
            }

            if (!tokenService.IsTokenExist(firstUser, secondUser))
            {
                tokenService.SetToken(firstUser, secondUser);
            }

            Console.WriteLine(ApplicationResources.EnterMessageText);
            string messageText = CaesarCipher.Encrypt(Console.ReadLine(), tokenService.GetToken(firstUser, secondUser));
            Message message = new Message(firstUser, secondUser, messageText);
            ApplicationStorage.messages.Add(message);
            messageFileService.WriteFile();
        }

        public void ShowDialogs()
        {
            messageFileService.ReadFile();
            SortMessagesByDateTime();
            string firstUser = userService.GetLoginUserName();
            Console.WriteLine(ApplicationResources.Dialogues);
            Console.WriteLine(ApplicationResources.ChooseUserToShowDialogue);
            userService.ShowUserList();
            object userId = userService.GetUserId();

            if (userId is null)
            {
                return;
            }

            int userIdValue = (int)userId;
            string secondUser = userService.GetUserNameById(userIdValue);
            ShowMessages(firstUser, secondUser);
        }

        private void ShowMessages(string firstUser, string secondUser)
        {
            for (int i = 0; i < ApplicationStorage.messages.Count; i++)
            {
                if ((ApplicationStorage.messages[i].Sender.Equals(firstUser) && ApplicationStorage.messages[i].Receiver.Equals(secondUser)) ||
                    (ApplicationStorage.messages[i].Sender.Equals(secondUser) && ApplicationStorage.messages[i].Receiver.Equals(firstUser)))
                {
                    Console.WriteLine($"{ApplicationResources.Id} {i}");
                    Console.WriteLine($"{ApplicationResources.Sender} {ApplicationStorage.messages[i].Sender}");
                    Console.WriteLine($"{ApplicationResources.Receiver} {ApplicationStorage.messages[i].Receiver}");
                    Console.WriteLine($"{ApplicationResources.MessageText} {CaesarCipher.Decrypt(ApplicationStorage.messages[i].MessageText, tokenService.GetToken(firstUser, secondUser))}");
                    Console.WriteLine();
                }
            }
        }

        public void DeleteMessage()
        {
            Console.WriteLine(ApplicationResources.ChooseUserToShowDialogue);
            userService.ShowUserList();
            object userId = userService.GetUserId();

            if (userId is null)
            {
                return;
            }

            int userIdValue = (int)userId;
            string secondUser = userService.GetUserNameById(userIdValue);
            ShowMessages(userService.GetLoginUserName(), secondUser);
            Console.WriteLine(ApplicationResources.ChooseIdMessageToDelete);
            int messageId = GetMessageId();
            messageFileService.DeleteMessageFile(new Message(ApplicationStorage.messages[messageId].Sender, secondUser, ApplicationStorage.messages[messageId].DateTime));
            ApplicationStorage.messages.RemoveAt(messageId);
        }

        private int GetMessageId()
        {
            int messageId;

            while (!int.TryParse(Console.ReadLine(), out messageId) || !IsCorrectMessageId(messageId))
            {
                Console.WriteLine(ApplicationResources.WrongInputText);
            }

            return messageId;
        }

        private bool IsCorrectMessageId(int messageId)
        {
            if(messageId < 0 || messageId >= ApplicationStorage.messages.Count)
            {
                return false;
            }

            return true;
        }

        private void SortMessagesByDateTime()
        {
            ApplicationStorage.messages.Sort((x, y) => DateTime.Compare(x.DateTime, y.DateTime));
        }
    }
}
