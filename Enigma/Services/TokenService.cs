﻿using Enigma.Implementations;
using Enigma.Models;
using Enigma.Storages;
using System;

namespace Enigma.Services
{
    public class TokenService
    {
        private TokenFileService tokenFileService = new TokenFileService();

        public int GetToken(string firstUser, string secondUser)
        {
            tokenFileService.ReadFile();

            for (int i = 0; i < ApplicationStorage.tokens.Count; i++)
            {
                if ((ApplicationStorage.tokens[i].FirstUser.Equals(firstUser) && ApplicationStorage.tokens[i].SecondUser.Equals(secondUser)) ||
                    (ApplicationStorage.tokens[i].FirstUser.Equals(secondUser) && ApplicationStorage.tokens[i].SecondUser.Equals(firstUser)))
                {
                    return ApplicationStorage.tokens[i].TokenValue;
                }
            }

            throw new ArgumentNullException(ApplicationResources.TokenIsNotExisted);
        }

        public void SetToken(string firstUser, string secondUser)
        {
            Console.WriteLine(ApplicationResources.EnterUniqueTokenText);
            int tokenValue = GetTokenValue();

            while (!IsUniqueToken(tokenValue))
            {
                Console.WriteLine(ApplicationResources.WrongInputText);
                tokenValue = GetTokenValue();
            }

            ApplicationStorage.tokens.Add(new Token(firstUser, secondUser, tokenValue));
            tokenFileService.WriteFile();
        }

        public bool IsUniqueToken(int tokenValue)
        {
            for (int i = 0; i < ApplicationStorage.tokens.Count; i++)
            {
                if (ApplicationStorage.tokens[i].TokenValue == tokenValue)
                {
                    return false;
                }
            }

            return true;
        }

        public bool IsTokenExist(string firstUser, string secondUser)
        {
            tokenFileService.ReadFile();

            for (int i = 0; i < ApplicationStorage.tokens.Count; i++)
            {
                if ((ApplicationStorage.tokens[i].FirstUser.Equals(firstUser) && ApplicationStorage.tokens[i].SecondUser.Equals(secondUser)) ||
                    (ApplicationStorage.tokens[i].FirstUser.Equals(secondUser) && ApplicationStorage.tokens[i].SecondUser.Equals(firstUser)))
                {
                    return true;
                }
            }

            return false;
        }

        private int GetTokenValue()
        {
            int tokenValue;

            while (!int.TryParse(Console.ReadLine(), out tokenValue))
            {
                Console.WriteLine(ApplicationResources.WrongInputText);
            }

            return tokenValue;
        }
    }
}
