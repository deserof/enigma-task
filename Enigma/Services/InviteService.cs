﻿using System;
using Enigma.Implementations;
using Enigma.Models;
using Enigma.Storages;

namespace Enigma.Services
{
    public class InviteService
    {
        private UserService userService = new UserService();
        private InviteFileService inviteFileService = new InviteFileService();

        public void SendInvite()
        {
            Console.WriteLine(ApplicationResources.ChooserUserThatYouWantToSendInvite);
            userService.ShowUserList();
            object userId = userService.GetUserId();

            if (userId is null)
            {
                return;
            }

            int userIdValue = (int)userId;
            string sender = userService.GetLoginUserName();
            string receiver = userService.GetUserNameById(userIdValue);

            if (IsInviteAccept(sender, receiver))
            {
                Console.WriteLine($"{receiver} {ApplicationResources.HasAcceptedInvite}");
                return;
            }

            if (IsInviteExist(sender, receiver))
            {
                Console.WriteLine(ApplicationResources.YouAlreadyHaveSentInviteOrYouHaveIncomingInvite);
                return;
            }

            Invite invite = new Invite(sender, receiver, false);
            ApplicationStorage.invites.Add(invite);
            inviteFileService.WriteFile();
        }

        public void AcceptInvite()
        {
            Console.WriteLine(ApplicationResources.ChooseInviteThatYouWantToAccept);
            ShowInvites();
            string receiver = userService.GetLoginUserName();
            object inviteId = GetInviteId();

            if (inviteId is null)
            {
                return;
            }

            int inviteIdValue = (int)inviteId;

            if (ApplicationStorage.invites[inviteIdValue].Receiver.Equals(receiver))
            {
                ApplicationStorage.invites[inviteIdValue].IsInviteAccpept = true;
                inviteFileService.WriteFile();
                Console.WriteLine(ApplicationResources.InviteIsAccepted);
                return;
            }

            Console.WriteLine(ApplicationResources.WrongInputText);
        }

        public bool IsInviteAccept(string sender, string receiver)
        {
            for (int i = 0; i < ApplicationStorage.invites.Count; i++)
            {
                if ((ApplicationStorage.invites[i].Sender.Equals(sender) && ApplicationStorage.invites[i].Receiver.Equals(receiver) &&
                    ApplicationStorage.invites[i].IsInviteAccpept == true) || (ApplicationStorage.invites[i].Sender.Equals(receiver) &&
                    ApplicationStorage.invites[i].Receiver.Equals(sender) && ApplicationStorage.invites[i].IsInviteAccpept == true))
                {
                    return true;
                }
            }

            return false;
        }

        private int? GetInviteId()
        {
            int inviteId;

            if (!int.TryParse(Console.ReadLine(), out inviteId) || !IsCorrectInviteId(inviteId))
            {
                Console.WriteLine(ApplicationResources.WrongInputText);
                return null;
            }

            return inviteId;
        }

        private bool IsCorrectInviteId(int inviteId)
        {
            if (inviteId < 0 || inviteId >= ApplicationStorage.invites.Count)
            {
                return false;
            }

            string receiver = userService.GetLoginUserName();

            for (int i = 0; i < ApplicationStorage.invites.Count; i++)
            {
                if (ApplicationStorage.invites[i].Receiver.Equals(receiver))
                {
                    return true;
                }
            }

            return false;
        }

        private bool IsInviteExist(string sender, string receiver)
        {
            for (int i = 0; i < ApplicationStorage.invites.Count; i++)
            {
                if ((ApplicationStorage.invites[i].Sender.Equals(sender) && ApplicationStorage.invites[i].Receiver.Equals(receiver) &&
                    ApplicationStorage.invites[i].IsInviteAccpept == false) || (ApplicationStorage.invites[i].Sender.Equals(receiver) &&
                    ApplicationStorage.invites[i].Receiver.Equals(sender) && ApplicationStorage.invites[i].IsInviteAccpept == false))
                {
                    return true;
                }
            }

            return false;
        }

        public void ShowInvites()
        {
            string receiver = userService.GetLoginUserName();

            for (int i = 0; i < ApplicationStorage.invites.Count; i++)
            {
                if (ApplicationStorage.invites[i].Receiver.Equals(receiver))
                {
                    Console.WriteLine($"{ApplicationResources.Id} {i}");
                    Console.WriteLine($"{ApplicationResources.Sender} {ApplicationStorage.invites[i].Sender}");
                    Console.WriteLine($"{ApplicationResources.Receiver} {ApplicationStorage.invites[i].Receiver}");
                    string invite = ApplicationStorage.invites[i].IsInviteAccpept == true ? ApplicationResources.InviteAccepted : ApplicationResources.InviteNotAccepted;
                    Console.WriteLine($"{invite}");
                    Console.WriteLine();
                }
            }
        }
    }
}
