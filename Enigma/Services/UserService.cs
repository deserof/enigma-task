﻿using System;
using Enigma.Storages;
using Enigma.Models;
using Enigma.Interfaces;
using Enigma.Implementations;
using System.IO;

namespace Enigma.Services
{
    public class UserService
    {
        private IFileService fileService = new BinaryFileService();

        public void DoAuthenticationUser()
        {
            fileService.ReadFile();
            bool isCorrectUser = false;
            string name = string.Empty;
            string password = string.Empty;

            while (!isCorrectUser)
            {
                Console.WriteLine(ApplicationResources.EnterNameText);
                name = GetInput();
                Console.WriteLine(ApplicationResources.EnterPasswordText);
                password = GetInput();

                if (!IsUserExist(name))
                {
                    isCorrectUser = true;
                }
            }

            ApplicationStorage.users.Add(new User(name, password));
            fileService.WriteFile();
            CreateUserFolder(name);
        }

        public void DoAuthorizationUser()
        {
            fileService.ReadFile();
            bool isCorrectUser = false;

            while (!isCorrectUser)
            {
                Console.WriteLine(ApplicationResources.EnterNameText);
                string name = GetInput();
                Console.WriteLine(ApplicationResources.EnterPasswordText);
                string password = GetInput();

                if (IsCorrectInput(name, password))
                {
                    isCorrectUser = true;
                    LoginUser(name);
                }
                else
                {
                    Console.WriteLine(ApplicationResources.WrongInputText);
                }
            }
        }

        public void Logout()
        {
            for (int i = 0; i < ApplicationStorage.users.Count; i++)
            {
                ApplicationStorage.users[i].Login = false;
            }
        }

        private void CreateUserFolder(string name)
        {
            Directory.CreateDirectory(ApplicationResources.UsersFolder + name);
        }

        private void LoginUser(string name)
        {
            for (int i = 0; i < ApplicationStorage.users.Count; i++)
            {
                if (ApplicationStorage.users[i].Name.Equals(name))
                {
                    ApplicationStorage.users[i].Login = true;
                }
            }
        }

        private bool IsUserExist(string name)
        {
            foreach (User item in ApplicationStorage.users)
            {
                if (item.Name == name)
                {
                    Console.WriteLine(ApplicationResources.UserExistedText);
                    return true;
                }
            }

            return false;
        }

        private string GetInput()
        {
            bool isCorrectInput = false;
            string text = string.Empty;

            while (!isCorrectInput)
            {
                text = Console.ReadLine();

                if (string.IsNullOrEmpty(text))
                {
                    Console.WriteLine(ApplicationResources.WrongInputText);
                }
                else
                {
                    isCorrectInput = true;
                }
            }

            return text;
        }

        private bool IsCorrectInput(string name, string password)
        {
            foreach (User item in ApplicationStorage.users)
            {
                if (item.Name.Equals(name) && item.Password.Equals(password))
                {
                    return true;
                }
            }

            return false;
        }

        public string GetUserNameById(int userId)
        {
            for (int i = 0; i < ApplicationStorage.users.Count; i++)
            {
                if (i == userId)
                {
                    return ApplicationStorage.users[i].Name;
                }
            }

            throw new ArgumentNullException(ApplicationResources.UserIsNotFoundText);
        }

        public string GetLoginUserName()
        {
            for (int i = 0; i < ApplicationStorage.users.Count; i++)
            {
                if (ApplicationStorage.users[i].Login == true)
                {
                    return ApplicationStorage.users[i].Name;
                }
            }

            throw new ArgumentNullException(ApplicationResources.LoginUserIsNotFoundText);
        }

        public int? GetUserId()
        {
            int userId;

            if (!int.TryParse(Console.ReadLine(), out userId) || !IsCorrectUserId(userId))
            {
                Console.WriteLine(ApplicationResources.WrongInputText);
                return null;
            }

            return userId;
        }

        private bool IsCorrectUserId(int userId)
        {
            if (userId < 0 || userId >= ApplicationStorage.users.Count)
            {
                return false;
            }

            return true;
        }

        public void ShowUserList()
        {
            Console.WriteLine(ApplicationResources.IdUserColumnText);

            for (int i = 0; i < ApplicationStorage.users.Count; i++)
            {
                Console.WriteLine(i + ". " + ApplicationStorage.users[i].Name);
            }
        }
    }
}
