﻿using System.Text;

namespace Enigma
{
    public static class CaesarCipher
    {
        private const string alfabet = ApplicationResources.Alfabet;

        public static string Encrypt(string text, int key)
        {
            StringBuilder encryptedText = new StringBuilder();

            for (int i = 0; i < text.Length; i++)
            {
                int index = alfabet.IndexOf(text[i]);

                if (index < 0)
                {
                    encryptedText.Append(text[i]);
                }
                else
                {
                    int codeIndex = (alfabet.Length + index + key) % alfabet.Length;
                    encryptedText.Append(alfabet[codeIndex]);
                }
            }

            return encryptedText.ToString();
        }

        public static string Decrypt(string text, int key)
        {
            StringBuilder decryptedText = new StringBuilder();

            for (int i = 0; i < text.Length; i++)
            {
                int index = alfabet.IndexOf(text[i]);

                if (index < 0)
                {
                    decryptedText.Append(text[i]);
                }
                else
                {
                    int codeIndex = (alfabet.Length + index - key) % alfabet.Length;
                    decryptedText.Append(alfabet[codeIndex]);
                }
            }

            return decryptedText.ToString();
        }
    }
}
